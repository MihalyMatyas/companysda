package com.companySda.companySda.components;

import com.companySda.companySda.model.Employee;
import com.github.javafaker.Faker;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CustomFakerEmployee {
    public List<Employee> createDummyEmployeeList() {
        List<Employee> employeeList = new ArrayList<>();

        Faker faker = new Faker();

        for (int i = 0; i < 500; i++) {
            Employee employee = new Employee();
            employee.setFirstName(faker.name().firstName());
            employee.setLastName(faker.name().lastName());
            employee.setEmail(faker.bothify("?????##@yahoo.com"));
            employee.setAddress(faker.address().fullAddress());
            employee.setPhone(faker.phoneNumber().phoneNumber());
            employee.setPersonalNumericCode(faker.number().randomNumber(10,true));
            employee.setHired(faker.bool().bool());

            employeeList.add(employee);
        }

        return employeeList;
    }
}
