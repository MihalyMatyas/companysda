package com.companySda.companySda.service.impl;

import com.companySda.companySda.dto.EmployeeCreateDto;
import com.companySda.companySda.dto.EmployeeResponseDto;
import com.companySda.companySda.dto.EmployeeFullDto;
import com.companySda.companySda.mapper.EmployeeMapper;
import com.companySda.companySda.model.Employee;
import com.companySda.companySda.repository.EmployeeRepository;
import com.companySda.companySda.service.EmployeeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public EmployeeResponseDto create(EmployeeCreateDto employeeCreateDto) {
        Employee employee = EmployeeMapper.employeeToEntity(employeeCreateDto);
        Employee savedEmployee = employeeRepository.save(employee);

        return EmployeeMapper.employeeToResponseDto(savedEmployee);
    }

    @Override
    public EmployeeFullDto findEmployeeById(int id) {
        Employee employee = employeeRepository.findById(id)
                .orElseThrow(()-> new RuntimeException("The employee with the given ID does not exist."));

        return EmployeeMapper.employeeToFullDto(employee);
    }

    @Override
    public EmployeeFullDto findEmployeeByFirstNameAndLastName(String firstName, String lastName) {
        Employee employee = employeeRepository.findByFirstNameAndLastName(firstName, lastName).orElseThrow(() -> new RuntimeException("Employee not found"));

        return EmployeeMapper.employeeToFullDto(employee);
    }

    @Override
    public String saveAllEmployees(List<Employee> employees) {
        List<Employee> employeeList = (List<Employee>) employeeRepository.saveAll(employees);

        return employeeList.size() + " employees were added.";
    }

    @Override
    public EmployeeFullDto findEmployeeByLastNameAndAddress(String lastName, String address) {
        Employee employee = employeeRepository.findByLastNameAndAddress(lastName, address)
                .orElseThrow(()-> new RuntimeException("Employee not found"));

        return EmployeeMapper.employeeToFullDto(employee);
    }
}
