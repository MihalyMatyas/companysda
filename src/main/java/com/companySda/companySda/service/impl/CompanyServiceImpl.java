package com.companySda.companySda.service.impl;

import com.companySda.companySda.dto.CompanyCreateDto;
import com.companySda.companySda.dto.CompanyFullDto;
import com.companySda.companySda.dto.CompanyResponseDto;
import com.companySda.companySda.exceptions.CompanyException;
import com.companySda.companySda.mapper.CompanyMapper;
import com.companySda.companySda.model.Company;
import com.companySda.companySda.repository.CompanyRepository;
import com.companySda.companySda.service.CompanyService;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CompanyServiceImpl implements CompanyService {

    //    @Autowired -> the field based dependency injection is not recommended
    private final CompanyRepository companyRepository;

    public CompanyServiceImpl(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @Override
    public CompanyResponseDto create(CompanyCreateDto companyCreateDto) {
        Company company = CompanyMapper.companyToEntity(companyCreateDto);
        Company savedCompany = companyRepository.save(company);

        return CompanyMapper.companyToResponseDto(savedCompany);
    }

    @Override
    public CompanyFullDto findCompanyByName(String name) {
        Company company = companyRepository.findByName(name)
                .orElseThrow(() -> new RuntimeException("Company not found"));

        return CompanyMapper.companyToFullDto(company);
    }

    @Override
    public CompanyFullDto findCompanyById(Integer id) {
        Company company = companyRepository.findById(id)
                .orElseThrow(() -> new CompanyException("Company with ID: " + id + " is not found"));

        return CompanyMapper.companyToFullDto(company);
    }

    @Override
    public CompanyFullDto findCompanyByNameAndRegistrationNumber(String name, Long registrationNumber) {
        Company company = companyRepository.findByNameAndRegistrationNumber(name, registrationNumber)
                .orElseThrow(() -> new RuntimeException("Company not found"));

        return CompanyMapper.companyToFullDto(company);
    }

    @Override
    public List<CompanyFullDto> findAll(Integer pageNumber, Integer pageSize, String sortBy) {

        Pageable pageable = PageRequest.of(pageNumber,pageSize, Sort.by(sortBy));

        List<CompanyFullDto> result = new ArrayList<>();
        companyRepository.findAll(pageable).forEach(entity -> {
            result.add(CompanyMapper.companyToFullDto(entity));
        });
        return result;
    }

    @Override
    public void saveAllCompanies(List<Company> listOfCompanies) {
        companyRepository.saveAll(listOfCompanies);
        System.out.println(listOfCompanies.size() + " companies was added");
    }

    @Override
    public List<CompanyFullDto> findAll() {
        List<CompanyFullDto> result = new ArrayList<>();
        companyRepository.findAll().forEach(entity -> {
            result.add(CompanyMapper.companyToFullDto(entity));
        });
        return result;
    }


}
