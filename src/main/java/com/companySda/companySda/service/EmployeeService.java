package com.companySda.companySda.service;

import com.companySda.companySda.dto.EmployeeCreateDto;
import com.companySda.companySda.dto.EmployeeFullDto;
import com.companySda.companySda.dto.EmployeeResponseDto;
import com.companySda.companySda.model.Employee;

import java.util.List;

public interface EmployeeService {

    EmployeeResponseDto create(EmployeeCreateDto employeeCreateDto);

    EmployeeFullDto findEmployeeByFirstNameAndLastName(String firstName, String lastName);

    String saveAllEmployees(List<Employee> employees);

    EmployeeFullDto findEmployeeByLastNameAndAddress(String lastName, String address);

    EmployeeFullDto findEmployeeById(int id);
}
