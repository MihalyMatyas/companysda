package com.companySda.companySda.service;

import com.companySda.companySda.dto.CompanyCreateDto;
import com.companySda.companySda.dto.CompanyFullDto;
import com.companySda.companySda.dto.CompanyResponseDto;
import com.companySda.companySda.model.Company;

import java.util.List;

public interface CompanyService {

    CompanyResponseDto create(CompanyCreateDto companyCreateDto);

    CompanyFullDto findCompanyByName(String name);

    CompanyFullDto findCompanyById(Integer id);

    CompanyFullDto findCompanyByNameAndRegistrationNumber(String name, Long registrationNumber);

    List<CompanyFullDto> findAll(Integer pageNumber, Integer pageSize, String sortBy);

    void saveAllCompanies(List<Company> listOfCompanies);

    List<CompanyFullDto> findAll();
}
