package com.companySda.companySda.repository;

import com.companySda.companySda.model.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Integer> {

    Optional<Employee> findByFirstNameAndLastName(String firstName, String lastName);

    Optional<Employee> findByLastNameAndAddress(String lastName, String address);
}
