package com.companySda.companySda.controller;

import com.companySda.companySda.dto.CompanyFullDto;
import com.companySda.companySda.model.LoginForm;
import com.companySda.companySda.service.CompanyService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class CompanyControllerThymeleaf {

    private final CompanyService companyService;

    @Value("${th.login.username}")
    private String userName;

    @Value("${th.login.password}")
    private String userPassword;

    public CompanyControllerThymeleaf(CompanyService companyService) {
        this.companyService = companyService;
    }

    @RequestMapping(value = "/")
    public String index() {
        return "start";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String getLoginForm(@ModelAttribute(name = "loginForm") LoginForm loginForm,
                               Model model) {
        String username = loginForm.getUsername();
        String password = loginForm.getPassword();
        if (username.equalsIgnoreCase(userName) && password.equalsIgnoreCase(userPassword)) {
            return "home";
        }
        model.addAttribute("Invalid credentials", true);
        return "start";
    }

    @RequestMapping(value = "showAll", method = RequestMethod.GET)
    public String showAllCompanies(Model model) {
        List<CompanyFullDto> companyList = companyService.findAll();
        model.addAttribute("companyList", companyList);

        return "companyTable";
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }
}
