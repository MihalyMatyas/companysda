package com.companySda.companySda.controller;


import com.companySda.companySda.components.CustomFakerCompany;
import com.companySda.companySda.dto.CompanyCreateDto;
import com.companySda.companySda.dto.CompanyFullDto;
import com.companySda.companySda.dto.CompanyResponseDto;
import com.companySda.companySda.model.Company;
import com.companySda.companySda.service.CompanyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/company")
@ControllerAdvice
public class CompanyController {

    //    @Autowired
    private final CompanyService companyService;

    //    @Autowired
    private final CustomFakerCompany customFakerCompany;

    public CompanyController(CompanyService companyService, CustomFakerCompany customFakerCompany) {
        this.companyService = companyService;
        this.customFakerCompany = customFakerCompany;
    }

    @GetMapping("/generateCompanies")
    public void generateCompanies() {
        List<Company> listOfCompanies = customFakerCompany.createDummyCompanyList();
        companyService.saveAllCompanies(listOfCompanies);
    }

    @PostMapping("/create")
    public ResponseEntity<CompanyResponseDto> createCompany(@Valid @RequestBody CompanyCreateDto companyCreateDto,
                                                            Principal principal) {

        System.out.println(principal.getName() + " has created a new company");

        CompanyResponseDto companyResponseDto = companyService.create(companyCreateDto);

        return ResponseEntity.ok(companyResponseDto);
    }

    @GetMapping("/findAll")
    public ResponseEntity<List<CompanyFullDto>> findAll(@RequestParam(defaultValue = "0") Integer pageNumber,
                                                        @RequestParam(defaultValue = "5") Integer pageSize,
                                                        @RequestParam(defaultValue = "id") String sortBy) {

        List<CompanyFullDto> listOfCompany = companyService.findAll(pageNumber,pageSize,sortBy);

        return ResponseEntity.ok(listOfCompany);
    }

    @GetMapping("/findByName")
    public ResponseEntity<CompanyFullDto> findByName(@RequestParam String companyName) {
        CompanyFullDto companyFullDto = companyService.findCompanyByName(companyName);
        return ResponseEntity.ok(companyFullDto);
    }

    @GetMapping("/findById")
    public ResponseEntity<CompanyFullDto> findById(@RequestParam Integer companyId) {
        CompanyFullDto companyFullDto = companyService.findCompanyById(companyId);
        return ResponseEntity.ok(companyFullDto);
    }

    @GetMapping("/findByNameAndRegistrationNumber")
    public ResponseEntity<CompanyFullDto> findByNameAndRegistrationNumber(@RequestParam String companyName,
                                                                          @RequestParam Long companyRegistrationNumber) {
        CompanyFullDto companyFullDto = companyService.findCompanyByNameAndRegistrationNumber(companyName, companyRegistrationNumber);
        return ResponseEntity.ok(companyFullDto);
    }


}
