package com.companySda.companySda.controller;

import com.companySda.companySda.components.CustomFakerEmployee;
import com.companySda.companySda.dto.EmployeeCreateDto;
import com.companySda.companySda.dto.EmployeeFullDto;
import com.companySda.companySda.dto.EmployeeResponseDto;
import com.companySda.companySda.model.Employee;
import com.companySda.companySda.service.EmployeeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/employee")
public class EmployeeController {

    //    @Autowired
    private final EmployeeService employeeService;

    //    @Autowired
    private final CustomFakerEmployee customFakerEmployee;

    public EmployeeController(EmployeeService employeeService, CustomFakerEmployee customFakerEmployee) {
        this.employeeService = employeeService;
        this.customFakerEmployee = customFakerEmployee;
    }

    @PostMapping("/create")
    public ResponseEntity<EmployeeResponseDto> createEmployee(@RequestBody EmployeeCreateDto employeeCreateDto) {
        EmployeeResponseDto employeeResponseDto = employeeService.create(employeeCreateDto);

        return ResponseEntity.ok(employeeResponseDto);
    }

    @GetMapping("/findByFirstNameAndLastName")
    public ResponseEntity<EmployeeFullDto> findByFirstNameAndLastName(@RequestParam String firstName,
                                                                      @RequestParam String lastName) {
        EmployeeFullDto employeeFullDto = employeeService.findEmployeeByFirstNameAndLastName(firstName, lastName);
        return ResponseEntity.ok(employeeFullDto);
    }

    @GetMapping("/generateEmployee")
    public ResponseEntity<String> generateEmployees() {
        List<Employee> listOfEmployees = customFakerEmployee.createDummyEmployeeList();

        return ResponseEntity.ok(employeeService.saveAllEmployees(listOfEmployees));
    }

    @GetMapping("findEmployeeByLastNameAndAddress")
    public ResponseEntity<EmployeeFullDto> findEmployeeByLastNameAndAddress(@RequestParam String lastName,
                                                                            @RequestParam String address) {
        EmployeeFullDto employeeFullDto = employeeService.findEmployeeByLastNameAndAddress(lastName, address);

        return ResponseEntity.ok(employeeFullDto);
    }

    @GetMapping("findEmployeeById")
    public ResponseEntity<EmployeeFullDto> findEmployeeById(@RequestParam Integer id) {
        EmployeeFullDto employeeFullDto = employeeService.findEmployeeById(id);

        return ResponseEntity.ok(employeeFullDto);
    }


}
