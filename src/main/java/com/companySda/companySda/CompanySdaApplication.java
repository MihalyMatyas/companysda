package com.companySda.companySda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompanySdaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompanySdaApplication.class, args);
	}

}
