package com.companySda.companySda.exceptions;

public class CompanyException extends RuntimeException {

    public CompanyException(String message) {
        super(message);
    }
}
