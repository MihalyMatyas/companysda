package com.companySda.companySda.config;

import com.companySda.companySda.components.CustomFakerCompany;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    public CustomFakerCompany customFakerCompany(){
        return new CustomFakerCompany();
    }
}
